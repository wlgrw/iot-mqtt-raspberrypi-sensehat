# MQTT: MQ Telemetry Transport

MQTT is a **machine-to-machine (M2M) "Internet of Things" connectivity protocol**.

It was designed as an **extremely lightweight publish/subscribe messaging transport**. It is useful for connections with remote locations where a small code footprint is required and/or network bandwidth is at a premium. **Near real time delivery of messages** in an efficient, cost-effective way, even in unreliable networks, is where MQTT excels.

Tutorials:

- [MQTT essentials](http://www.hivemq.com/blog/mqtt-essentials-wrap-up)

- [C and C++ code libs, Mosquitto lib](http://www.mosquitto.org)

## Public MQTT brokers

Several MQTT brokers are publically available. They can be used in webpages. It is necessarry to connect them by the **WebSocket protocol**.

The WebSocket protocol enables full-duplex interaction between a browser and a web server with lower overheads,facilitating (near) real-time data transfer.

Eclipse Paho MQTT Javascript WebSockets implementation:

- [Javascript MQTT library](http://www.hivemq.com/blog/mqtt-client-library-encyclopedia-paho-js)

Always check if the correct port number is used. This web app does not use any encryption. Every MQTT cient using the same broker must use **an unique MQTT client identifier**.

Some example MQTT broker URLs:

>ws://broker.hivemq.com:8000/mqtt
>
>ws://iot.eclipse.org:80/ws

Public brokers are not always up! Because MQTT is standardized we can easily choose another broker.

Some example URLs for connecting to a **local MQTT broker**, using the websocket protocol port 9001:

>ws://localhost:9001/ws
>
>ws://127.0.0.1:9001/ws
>
>ws://192.168.20.123:9001/ws

---

## Responsive web page development

HTML5, CSS3, JavaScript and the **Bootstrap framework** are used for developing the basic MQTT webapp:

- [HTML5 tutorial](https://www.w3schools.com/html/default.asp)
- [JavaScript tutorial](https://www.w3schools.com/Js/)
- [Bootstrap](http://getbootstrap.com/)
