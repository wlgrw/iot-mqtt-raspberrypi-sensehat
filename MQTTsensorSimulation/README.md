# MQTT sensor simulation bash script

Command example to show usage:

```bash
./sensorSimulation.sh

 Script: ./sensorSimulation.sh v1.1 will connect to a MQTT broker and simulate
           a periodic sensor signal.

-- Usage: ./sensorSimulation.sh <url broker> <signal type> <Ts> <topic>

   Examples:
   <url broker>:  broker.hivemq.com
   <signal type>: swt (sawtooth) tra (triangle)
   <Ts>:          sample time [sec]
   <topic>:       MQTT topic without root text (=ESEiot/1920sep/<hostname>/)

```

Command example to start simulation:

```bash
./sensorSimulation.sh broker.hivemq.com swt 10 sensor
--- Simulation started: Ts = 10 sec, signal type = swt, topic = 'ESEiot/1920sep/Eos/sensor'
--- mosquitto_pub -h broker.hivemq.com -t ESEiot/1920sep/Eos/sensor -m 0
--- mosquitto_pub -h broker.hivemq.com -t ESEiot/1920sep/Eos/sensor -m 10
--- mosquitto_pub -h broker.hivemq.com -t ESEiot/1920sep/Eos/sensor -m 20
```
