#!/bin/bash

version="1.2"

if [ $# -ne 4 ];
then
  echo
  echo "-- Script: $0 v$version will connect to a MQTT broker and simulate"
  echo "           a periodic sensor signal."
  echo
  echo "-- Usage: $0 <url broker> <signal type> <Ts> <topic>"
  echo
  echo "   Examples:"
  echo "   <url broker>:  broker.hivemq.com"
  echo "   <signal type>: swt (sawtooth) tra (triangle)"
  echo "   <Ts>:          sample time [sec]"
  echo "   <topic>:       MQTT topic without root text (=ESEiot/2021sep/<hostname>/)"
  echo
  exit 1
fi

sensorValue=0
sensorValueStep=1
readonly SENSOR_MAX=100
readonly SENSOR_MIN=-100
readonly ROOT_TOPIC="ESEiot/2021sep/"$HOSTNAME

broker=$1
signaltype=$2
sampleTime=$3
topic=$ROOT_TOPIC"/"$4

sawTooth()
{
   sensorValue=$((sensorValue+10))
   if [ $sensorValue -gt $SENSOR_MAX ]
   then
     sensorValue=$SENSOR_MIN
   fi
}

triangle()
{
   if [ $sensorValue -ge $SENSOR_MAX ]
   then
     sensorValueStep=-1
   fi
   if [ $sensorValue -le $SENSOR_MIN ]
   then
     sensorValueStep=1
   fi
   sensorValue=$((sensorValue + sensorValueStep *1 0))
}

echo "--- Simulation started: Ts = $sampleTime sec, signal type = $signaltype, topic = '$topic'"
while true
do
   echo "--- mosquitto_pub -h $broker -t $topic -m $sensorValue"
   mosquitto_pub -h "$broker" -t "$topic" -m "$sensorValue"
   case "$signaltype" in
      swt) sawTooth
         ;;
      tra) triangle
         ;;
      *)
         echo "** ERROR signal type unknown"
         exit 2
         ;;
   esac
   sleep $sampleTime
done
