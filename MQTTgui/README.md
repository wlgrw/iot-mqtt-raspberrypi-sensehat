# Qt C++ MQTT GUI: *qmqtt*

## Build and install the *qmqtt* library

Commands:

    sudo apt update
    sudo apt upgrade

    sudo apt install qt5-default
    cd .......MQTTgui/qmqtt-master/src

    qmake               # avoid the warnings
    make -j4
    sudo make install

If make fails, retry again and use:

    make clean
    make -j4
    sudo make install

## Build management: *qmqtt-client.pro* adds the lib *qmqtt* to the linking step

The compilation of qmqtt-client-master needs only the library name of
qmqtt in *qmqtt-client.pro*. A full path name is no longer necessary:

    LIBS += -lqmqtt

If the library *qmqtt* is not found this will result in a linker error.

## Building Qt project with Visual Studio Code

Open VSCode integrated terminal in *qmqtt-client-master* directory:

    qmake
    make -j4

    ./qmqtt-client

## MQTTgui

![](MQTTgui.png)
