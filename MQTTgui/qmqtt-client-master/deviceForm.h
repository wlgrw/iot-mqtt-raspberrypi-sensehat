#ifndef DEVICEFORM_H
#define DEVICEFORM_H

#include <QWidget>
// ##
#include "mqttform.h"

namespace Ui {
class DeviceForm;
}

// ##
//class DeviceForm : public QWidget
class DeviceForm : public MqttForm
{
    Q_OBJECT

public:
    explicit DeviceForm(QWidget *parent = nullptr);
    ~DeviceForm();

private slots:
    void on_StartPB_clicked();
    void on_StopPB_clicked();
    void on_ToggleLedPB_clicked();

private:
    Ui::DeviceForm *ui;
    int count_{0};
    int toggleLed_{0};
};

#endif // DEVICEFORM_H
