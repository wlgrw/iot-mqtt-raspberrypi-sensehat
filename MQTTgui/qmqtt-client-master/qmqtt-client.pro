#-------------------------------------------------
#
# Project created by QtCreator 2013-03-22T21:02:13
#
#-------------------------------------------------

QT += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
#STATECHARTS += device.scxml

TARGET = qmqtt-client
TEMPLATE = app

INCLUDEPATH += $$PWD/../qmqtt-master/src/mqtt

message("Build directory = " $$OUT_PWD)

LIBS += -lqmqtt

SOURCES += main.cpp \
    mainwindow.cpp \
    connform.cpp \
    pubform.cpp \
    subform.cpp \
    deviceForm.cpp
    mainwindow.cpp

HEADERS += mainwindow.h \
    connform.h \
    pubform.h \
    subform.h \
    mqttform.h \
    deviceForm.h

FORMS += mainwindow.ui \
    connform.ui \
    pubform.ui \
    subform.ui \
    deviceForm.ui

DISTFILES += \
    device.qmodel
