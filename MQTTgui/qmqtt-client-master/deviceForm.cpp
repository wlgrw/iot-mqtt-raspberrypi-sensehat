#include "deviceForm.h"
#include "ui_deviceForm.h"

#include <iostream>

DeviceForm::DeviceForm(QWidget *parent) :
    // ##
    // QWidget(parent),
    MqttForm(parent),
    ui(new Ui::DeviceForm)
{
    ui->setupUi(this);
}

DeviceForm::~DeviceForm()
{
    delete ui;
}

void DeviceForm::on_StartPB_clicked()
{
    QMQTT::Message msg(0, "ESEiot/2021sep/MQTTdevice", QString("Start").toUtf8());
    _client->publish(msg);
}

void DeviceForm::on_StopPB_clicked()
{
    QMQTT::Message msg(0, "ESEiot/2021sep/MQTTdevice", QString("Stop").toUtf8());
    _client->publish(msg);
}

void DeviceForm::on_ToggleLedPB_clicked()
{
    if(toggleLed_ == 0)
    {
        QMQTT::Message msg(0, "ESEiot/2021sep/WebApp/Demo/ledgreen", QString("0").toUtf8());
        _client->publish(msg);
         toggleLed_ = 1;
    }
    else
    {
        QMQTT::Message msg(0, "ESEiot/2021sep/WebApp/Demo/ledgreen", QString("1").toUtf8());
        _client->publish(msg);
        toggleLed_ = 0;
    }
}
